class ApplicationController  < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  prepend_before_action :set_current_user
  #append_before_action :user_check
  before_action :set_locale

  # Setup parameters allowed for passthrough to active record
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up,
                                      keys: [:username, :first_name, :last_name,
                                      :faculty, :email, :password, :password_confirmation])
    devise_parameter_sanitizer.permit(:sign_in, keys: [:login, :username, :email, :password, :remember_me])
    devise_parameter_sanitizer.permit(:account_update,
                                      keys: [:username, :email, :first_name, :last_name, :faculty,
                                      :password, :password_confirmation, :current_password])
  end

  # Create virtual guest user in database when site is visited
  def user_check
    current_or_guest_user if current_or_guest_user.blank?
  end

  def admin_only
    unless current_or_guest_user.admin?
      redirect_back_or_default(root_path, :alert => "Sorry! You do not have permission to access this content!.")
    end
  end

  def redirect_back_or_default(default = root_path, *options)
    tag_options = {}
    options.first.each { |k,v| tag_options[k] = v } unless options.empty?
    @back = request.referer
    redirect_to (request.referer.present? ?  @back : default), tag_options
  end

  def set_current_user
    @current_user = current_or_guest_user
  end


  def set_locale
    if cookies[:my_locale] && I18n.available_locales.include?(cookies[:my_locale].to_sym)
      l = cookies[:my_locale].to_sym
    else
      l = I18n.default_locale
      cookies.permanent[:my_locale] = l
    end
    I18n.locale = l
  end
end
