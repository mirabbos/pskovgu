class Users::ProfilesController < ApplicationController
  before_action :set_profile, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /profiles
  def index
    @profiles = Profile.where(:user_id => current_user.id)
  end

  def new
    @profile = Profile.new
  end

  # GET /profiles/1
  def show
    #render json: @profile
  end

  #Edit /profiles/:id/
  def edit

  end

  # POST /profiles
  def create
    @profile = Profile.new(profile_params)
    @profile.user_id = @current_user.id
    @profile.faculty = @current_user.faculty
    if @profile.save
      flash[:notice] = "Портфолио было создано успешно!"
      case @current_user.faculty
        when "P"
          redirect_to info_path
        when "M"
          redirect_to math_path
        when "F"
          redirect_to physics_path
        when "MC"
          redirect_to math_cs_path
      end
    else
      flash[:error] = "При создании Портфолио произошла ошибка. Пожалуйста повторите свои действия!"
      case @current_user.faculty
        when "P"
          redirect_to info_path
        when "M"
          redirect_to math_path
        when "F"
          redirect_to physics_path
        when "MC"
          redirect_to math_cs_path
      end
    end
  end

  # PATCH/PUT /profiles/1
  def update
    if @profile.update(profile_params)
      flash[:notice] = "Ваш Портфолио был успешно обновлен!"
      redirect_back_or_default(root_path)
      #redirect_to request_user_show_path(@request)
    else
      flash[:error] = "Возникли ошибки при обновлении!"
      redirect_back_or_default(root_path, :alert => "Some thing wrong!.")
    end
  end

  # DELETE /profiles/1
  def destroy
    @profile.destroy
    redirect_to profiles_path
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_profile
    @profile = Profile.find(params[:profile_id])
  end

  # Only allow a trusted parameter "white list" through.
  def profile_params
    if Rails::VERSION::MAJOR < 4
      params[:profile].permit(:_destroy)
    else
      protected_attrs =  ["created_at", "updated_at"]
      params.require(:profile).permit(Profile.new.attributes.keys - protected_attrs, :_destroy)
    end
  end
end
