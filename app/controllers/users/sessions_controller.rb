class Users::SessionsController < Devise::SessionsController
  prepend_before_action :require_no_authentication, only: [ :new, :create ]
  prepend_before_action :allow_params_authentication!, only: :create
  prepend_before_action :verify_signed_out_user, only: :destroy
  skip_before_action :set_current_user, except: [:new]
  before_action :require_login

  #prepend_before_action only: [ :create, :destroy ] { request.env["devise.skip_timeout"] = true }

  # GET /resource/sign_in
  def new
    #@header_title = t('users.sessions.header_title')
    self.resource = resource_class.new(sign_in_params)
    clean_up_passwords(resource)
    respond_with(resource, serialize_options(resource))
  end  # End

  #POST /resource/sign_in
  def create
    @alert_message = "Авторизация прошла успешна!!!"
    auth_options = { :scope => :user}
    self.resource = warden.authenticate!(auth_options)
    unless current_user.username.nil?
      flash[:notice] = @alert_message
    end
    #devise code
    set_flash_message :success,  :signed_in if is_flashing_format?
    yield resource if block_given?
    respond_with resource, location: after_sign_in_path_for(resource)
  end


  #GET /resource/sign out
  def sdestroy
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    set_flash_message :success, :signed_out if signed_out && is_flashing_format?
    yield if block_given?
    respond_to_on_destroy
  end

  #DELETE /resource/ssign_out
  def destroy
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    set_flash_message :success, :signed_out if signed_out && is_flashing_format?
    yield if block_given?
    respond_to_on_destroy
  end

  # protected

  # You can put the params you want to permit in the empty array.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end

  private

  def require_login
    unless current_user
      flash[:error] = "Авторизация не прошла. Пожалуйста проверьте ваш Логин и Пароль!"
      redirect_to root_path
    end
  end
  # Check if there is no signed in user before doing the sign out.
  #
  # If there is no signed in user, it will set the flash message and redirect
  # to the after_sign_out path.
  def verify_signed_out_user
    if all_signed_out?
      set_flash_message :warning, :already_signed_out if is_flashing_format?

      respond_to_on_destroy
    end
  end

  def all_signed_out?
    users = Devise.mappings.keys.map { |s| warden.user(scope: s, run_callbacks: false) }

    users.all?(&:blank?)
  end

  def respond_to_on_destroy
    # We actually need to hardcode this as Rails default responder doesn't
    # support returning empty response on GET request
    respond_to do |format|
      format.all { head :no_content }
      format.any(*navigational_formats) { redirect_to after_sign_out_path_for(resource_name) }
    end
  end


  def after_sign_out_path_for(resource)
    if resource.nil?
      root_url
    else
      root_url
    end
  end

end