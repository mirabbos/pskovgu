class PublicController < ApplicationController
  def index

  end

  def info_faculty
    @info_profiles = Profile.where(:faculty => "P")
  end

  def math_faculty
    @math_profiles = Profile.where(:faculty => "M")
  end

  def physics_faculty
    @physics_profiles = Profile.where(:faculty => "F")
  end

  def math_cs_faculty
    @math_cs_profiles = Profile.where(:faculty => "MC")
  end

end