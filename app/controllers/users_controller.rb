class UsersController < ApplicationController
  before_action :authenticate_user!, :except => [:show]
  before_action :set_user, only: [:show, :edit, :update, :destroy]


  def index
    if params[:search]
      @users = User.search(params[:search])
    else
      @users = User.with_role(:user)
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.encrypted_password = User.new.send(:password_digest, user_params[:password])
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def show
    #@header_title = @user.username + t('users.show.header_title') unless @user.username.nil?
  end

  def update
    if @user.update(user_params)
      redirect_to account_path, notice: "User was successfully updated."
    else
      format.html render :edit, notice: "Unable to update user."
    end
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def account
    @user = current_or_guest_user
    @profiles = Profile.where(:user_id => @user)
    #@driver = Driver.find_by(:user_id => current_user.id)
    #@business = Business.find_by(:user_id => current_user.id)
    #@ships = Ship.all
    @header_title = t('users.account.header_title')
    case @user.faculty
      when "P"
        @faculty_name = "Прикладная информатика"
      when "M"
        @faculty_name = "Математика"
      when "F"
        @faculty_name = "Физико-Информатика"
      when "MC"
        @faculty_name = "Математика и компьютерные науки"
    end
  end

  def upgrade

  end

  private


  # Use callbacks to share common setup or constraints between actions.
  # Use callbacks to share common setup or constraints between actions.

  def set_user
    @user = current_or_guest_user
  end

  # Use strong_parameters for attribute whitelisting
  # Be sure to update your create() and update() controller methods.
  def user_params
    if Rails::VERSION::MAJOR < 4
      params[:user].permit(:_destroy)
    else
      protected_attrs =  ["created_at", "updated_at"]
      params.require(:user).permit(User.new.attributes.keys - protected_attrs,:_destroy, :password, role_ids:[] )
    end
  end

end