var Vgyme = {
    user_key: "F3bIZUnPQzsCDaqs4vDRCc7JoO76VmU8",
    image_url: "",
    upload_file: function (file) {
        this.image_url = "";
        $.ajaxSetup({"async": false});
        var data = new FormData();
        data.append('file', file);
        data.append('userkey',this.user_key);
        $.ajax({
            url: "https://vgy.me/upload",
            method: 'POST',
            dataType: 'json',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST' // For jQuery < 1.9
        }).done(function (data) {
            // For Single upload
            if(typeof data.image != "undefined")
            {
                Vgyme.image_url = data.image;
                //console.log( "Upload url result : " + data.image);
            }
        }).fail(function (data) {
            //console.log( "Vgy.Me Upload request failed! " + data.error);
        });
    }
};