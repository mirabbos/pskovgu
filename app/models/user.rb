class User < ApplicationRecord

  # Include default devise modules.
  devise :database_authenticatable, :registerable,:confirmable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, authentication_keys: [:login]
  include DeviseTokenAuth::Concerns::User
  #:confirmable,
  has_one :device, :dependent => :destroy
  has_many :profiles, :dependent => :destroy

  rolify
  before_create :assign_default_role
  before_destroy :destroy_all_roles


  # Virtual attribute for authenticating by either username or email
  # This is in addition to a real persisted field like 'username'
  attr_accessor :login


  validates :first_name,
            :presence => true
  validates :last_name,
            :presence => true
  validates :email,
            :presence => true
  validates :username,
            :presence => true,
            :uniqueness => {
                :case_sensitive => false
            },
            :length => {:minimum => 3, :maximum => 15}
  validates :encrypted_password,
            :presence => true
  validates :faculty,
            :presence => true
  def email_required?
    provider == 'email' ? super : false
  end

  def email_changed?
    provider == 'email' ? super : false
  end

  def will_save_change_to_email?
    provider == 'email' ? super : false
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions.to_h).first
    end
  end

  #Cleanup guests based on username and last name is NULL and firstname is Guest
  def self.clean_guests
    #Gather all guest users
    @guests = User.all.where(username: nil,last_name: nil,first_name: 'Guest')
    @guests_count = @guests.count

    #Attempt to stay within heroku 10k rows
    #if @guests_count < 100
    @guests = @guests.where('updated_at <= ?',Time.zone.now-1.days)
    @guests_count = @guests.count
    #end

    if @guests_count > 0

      @guests.each do |guest|
        guest.destroy
      end

      return "#{@guests_count} guest users have been cleaned!"

    else

      return "No extra guest users to clean!"


    end

  end


  def name
    "#{self.email}"
  end

  #User login options support
  def login=(login)
    @login = login
  end

  def login
    @login || self.username || self.email
  end

  #User Role Management
  def assign_default_role
    self.user! if self.roles.blank?
    self.remove_role(:guest) if self.guest?
  end

  def user? #Check if user
    self.has_role? :user
  end

  def user! #Set as user
    self.add_role(:user) unless self.user?
  end

  def guest? #Check if guest
    self.has_role? :guest
  end

  def guest! #Set as guest
    self.remove_role(:user) if self.user?
    self.add_role(:guest) unless self.guest?
  end

  def driver? #Check if driver
    self.has_role? :driver
  end

  def driver! #Set driver
    self.remove_role(:guest) if self.guest?
    self.add_role(:driver) unless self.driver?
  end

  def business? #Check if business
    self.has_role? :business
  end

  def business! #Set business
    self.remove_role(:guest) if self.guest?
    self.add_role(:business) unless self.business?
  end


  def admin? #Check if admin
    self.has_role? :admin
  end

  def admin! #Set admin
    self.remove_role(:guest) if self.guest?
    self.add_role(:user) unless self.user?
    self.add_role(:admin) unless self.admin?
  end

  #Destroy users roles
  def destroy_all_roles
    if self.roles.count > 0
      self.roles.each do |role|
        self.remove_role role.name
      end
    end
  end

  protected

  # I disable this method because I don't use the confirmable Devise module
  def confirmation_required?
    false
  end
  #Checking for admin
  def admin_only
    unless current_user.admin?
      redirect_to root_path, :alert => "Access denied."
    end
  end


end
