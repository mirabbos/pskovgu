class UserRole < ApplicationRecord
  self.table_name = "users_roles"
  belongs_to :roles
  belongs_to :users

end