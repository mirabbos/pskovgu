class Profile < ApplicationRecord
  belongs_to :user

  nilify_blanks
  # Backend validations
  validates :first_name,
            :presence => true
  validates :last_name,
            :presence => true
  validates :patronymic,
            :presence => true
  validates :address,
            :presence => true
  validates :telephone,
            :presence => true
  validates :birth_date,
            :presence => true
  
end