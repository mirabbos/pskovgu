SimpleNavigation::Configuration.run do |navigation|
  navigation.selected_class = 'active'
  navigation.items do |primary|
    primary.dom_class = 'nav scroll'
    primary.item :home, t("layouts.navigation.home").html_safe, root_path, html: { :class => 'nav__list', :id=>''}, link_html: { :class => ' nav__link' }

  end
end
