Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  #Path to root page
  root to: 'public#index', as: 'root'
  #User handling routes
  devise_for :users, skip: [:sessions, :passwords, :confirmations, :registrations, :unlocks]
  match 'lang/:locale', to: 'visitors#change_locale', as: :change_locale, via: [:get]
  #Update user profile
  #get '/user/edit', to: 'users/users#edit', as: 'user_edit'  
  put '/user/edit', to: 'users#update', as: 'user_update'
  ###############################################################
  devise_scope :user do
    get '/signin', to: 'users/sessions#new', as: 'new_user_session'
    post '/signin', to: 'users/sessions#create', as: 'user_session'
    get '/signout', to: 'users/sessions#sdestroy', as: 'secure_destroy_user_session'
    delete '/signout', to: 'users/sessions#destroy', as: 'destroy_user_session'
    # registrations
    get '/signup', to: 'users/registrations#new', as: 'new_user_registration'
    post '/signup', to: 'users/registrations#create', as: 'user_registration'
    # user accounts
    scope '/account' do
      # confirmation
      get '/verification', to: 'users/confirmations#verification_sent', as: 'user_verification_sent'
      get '/confirm', to: 'users/confirmations#show', as: 'user_confirmation'
      get '/confirm/resend', to: 'users/confirmations#new', as: 'new_user_confirmation'
      post '/confirm', to: 'users/confirmations#create'
      # passwords
      get '/reset-password', to: 'users/passwords#new', as: 'new_user_password'
      get '/reset-password/change', to: 'users/passwords#edit', as: 'edit_user_password'
      put  '/reset-password', to: 'users/passwords#update', as: 'user_password'
      post '/reset-password', to: 'users/passwords#create'
      # unlocks
      post '/unlock', to: 'users/unlocks#create', as: 'user_unlock'
      get '/unlock/new', to: 'users/unlocks#new', as: 'new_user_unlock'
      get '/unlock', to: 'users/unlocks#show'
      # settings & cancellation
      get '/cancel', to: 'users/registrations#cancel', as: 'cancel_user_registration'
      get '/settings', to: 'users/registrations#edit', as: 'edit_user_registration'
      put '/settings', to: 'users/registrations#update', as: 'update_user_registration'
      # account deletion
      delete '/delete', to: 'users/registrations#destroy', as: 'destroy_user_registration'
    end
  end
  #User account paths
  get '/account', to: 'users#account', as: 'account'
  get '/account/edit_news', to: 'users/contents#edit_news'
  put '/account/update_news', to: 'users/contents#update_news'
  ################################################################
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      mount_devise_token_auth_for 'User', at: 'auth',
                                  controllers: {
                                      sessions: 'api/v1/devise_token_auth/sessions',
                                      registrations: 'api/v1/devise_token_auth/registrations'
                                  }
      resources :devices, only: [ :create ]
      resources :locations, only: [ :create ]
      post '/analyze', to: 'alpr#analyze', as: 'alpr_analyze'
    end
  end
  #profiles index path
  get '/profiles', to: 'users/profiles#index', as: 'profiles'
  scope :users do
    #User profile new path
    get '/profile/new', to: 'users/profiles#new', as: 'profile_new'
    post '/profile/new', to: 'users/profiles#create', as: 'profile_create'
    #user profile edit & delete path
    resources :profiles, only:[] do
      #User profiles update and destroy paths
      get '/edit', to: 'users/profiles#edit', as: 'edit'
      put '/edit', to: 'users/profiles#update', as: 'update'
      delete '/delete', to: 'users/profiles#destroy', as: 'destroy'
    end
  end

  get '/applied_informatics', to: 'public#info_faculty', as: 'info'
  get '/mathematics', to: 'public#math_faculty', as: 'math'
  get '/physics_informatics', to: 'public#physics_faculty', as: 'physics'
  get '/math_computer_engineering', to: 'public#math_cs_faculty', as: 'math_cs'

end
