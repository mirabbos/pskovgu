require 'test_helper'

class DocksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @dock = docks(:one)
  end

  test "should get index" do
    get docks_url, as: :json
    assert_response :success
  end

  test "should create dock" do
    assert_difference('Dock.count') do
      post docks_url, params: { dock: { arrival: @dock.arrival, departure: @dock.departure, name: @dock.name, ship_id: @dock.ship_id } }, as: :json
    end

    assert_response 201
  end

  test "should show dock" do
    get dock_url(@dock), as: :json
    assert_response :success
  end

  test "should update dock" do
    patch dock_url(@dock), params: { dock: { arrival: @dock.arrival, departure: @dock.departure, name: @dock.name, ship_id: @dock.ship_id } }, as: :json
    assert_response 200
  end

  test "should destroy dock" do
    assert_difference('Dock.count', -1) do
      delete dock_url(@dock), as: :json
    end

    assert_response 204
  end
end
