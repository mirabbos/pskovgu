require "application_system_test_case"

class DevicesTest < ApplicationSystemTestCase
  setup do
    @device = devices(:one)
  end

  test "visiting the index" do
    visit devices_url
    assert_selector "h1", text: "Devices"
  end

  test "creating a Device" do
    visit devices_url
    click_on "New Device"

    fill_in "Accuracy", with: @device.accuracy
    fill_in "Altitude", with: @device.altitude
    fill_in "Altitudeaccuracy", with: @device.altitudeAccuracy
    fill_in "Heading", with: @device.heading
    fill_in "Latitude", with: @device.latitude
    fill_in "Longitude", with: @device.longitude
    fill_in "Speed", with: @device.speed
    fill_in "User", with: @device.user_id
    click_on "Create Device"

    assert_text "Device was successfully created"
    click_on "Back"
  end

  test "updating a Device" do
    visit devices_url
    click_on "Edit", match: :first

    fill_in "Accuracy", with: @device.accuracy
    fill_in "Altitude", with: @device.altitude
    fill_in "Altitudeaccuracy", with: @device.altitudeAccuracy
    fill_in "Heading", with: @device.heading
    fill_in "Latitude", with: @device.latitude
    fill_in "Longitude", with: @device.longitude
    fill_in "Speed", with: @device.speed
    fill_in "User", with: @device.user_id
    click_on "Update Device"

    assert_text "Device was successfully updated"
    click_on "Back"
  end

  test "destroying a Device" do
    visit devices_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Device was successfully destroyed"
  end
end
