# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_12_154423) do

  create_table "devices", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "platform"
    t.string "installationId"
    t.string "expoVersion"
    t.string "appOwnership"
    t.string "systemName"
    t.string "systemVersion"
    t.string "deviceName"
    t.string "deviceYearClass"
    t.string "sessionId"
    t.string "isDevice"
    t.string "udid"
    t.index ["user_id"], name: "index_devices_on_user_id"
  end

  create_table "locations", force: :cascade do |t|
    t.string "speed"
    t.string "longitude"
    t.string "latitude"
    t.string "accuracy"
    t.string "heading"
    t.string "altitude"
    t.string "altitudeAccuracy"
    t.integer "device_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["device_id"], name: "index_locations_on_device_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.text "description"
    t.string "image_url"
    t.boolean "is_active", default: true
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "patronymic"
    t.string "faculty"
    t.string "gender"
    t.string "telephone"
    t.string "address"
    t.string "uploaded_files"
    t.datetime "birth_date"
    t.string "workplace"
    t.string "old_workplace"
    t.string "study_place"
    t.string "hobbies"
    t.string "portfolio_url"
    t.string "achievements"
    t.string "work_date"
    t.string "old_work_date"
    t.string "last_work_date"
    t.string "first_position"
    t.string "second_position"
    t.string "third_position"
    t.string "first_workplace"
    t.string "second_workplace"
    t.string "third_workplace"
    t.string "vk"
    t.string "facebook"
    t.string "instagram"
    t.string "course_year"
    t.string "technical_skills"
    t.string "cultural_skills"
    t.string "scientific_skills"
    t.string "general_skills"
    t.string "achievement_docs"
    t.index ["user_id"], name: "index_profiles_on_user_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.integer "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["name"], name: "index_roles_on_name"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.text "tokens"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "username"
    t.string "first_name"
    t.string "last_name"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "profile_image_url"
    t.string "faculty"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

end
