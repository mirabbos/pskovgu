# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

@default_roles = ["user","admin"]

@default_roles.each do |role|
  Role.find_or_create_by(:name => role)
end

@admin = User.find_or_create_by!(email: ENV['ADMIN_EMAIL']) do |admin|
  admin.username = ENV['ADMIN_USERNAME']
  admin.password = ENV['ADMIN_PASSWORD']
  admin.first_name = ENV['ADMIN_FIRST_NAME']
  admin.last_name = ENV['ADMIN_LAST_NAME']
  admin.faculty = ENV['ADMIN_FACULTY']
end

!@admin.confirmed? ? @admin.confirm : ""
!@admin.admin? ? @admin.add_role(:admin)  : ""