class AddColumnToProfiles < ActiveRecord::Migration[5.2]
  def change
    remove_column :profiles, :name
    add_column :profiles, :birth_date, :datetime
    add_column :profiles, :workplace, :string
    add_column :profiles, :old_workplace, :string
    add_column :profiles, :study_place, :string
    add_column :profiles, :hobbies, :string
    add_column :profiles, :portfolio_url, :string
    add_column :profiles, :achievements, :string
  end
end
