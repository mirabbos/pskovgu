class RemoveColumnFaculty < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :faculty
    add_column :users, :faculty, :string
  end
end
