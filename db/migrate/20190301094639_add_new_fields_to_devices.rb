class AddNewFieldsToDevices < ActiveRecord::Migration[5.2]
  def change
    add_column :devices, :application_name, :string
    add_column :devices, :battery_level, :string
    add_column :devices, :brand, :string
    add_column :devices, :build_number, :string
    add_column :devices, :bundle_id, :string
    add_column :devices, :carrier, :string
    add_column :devices, :device_country, :string
    add_column :devices, :device_id, :string
    add_column :devices, :device_locale, :string
    add_column :devices, :device_name, :string
    add_column :devices, :font_scale, :string
    add_column :devices, :free_disk_storage, :string
    add_column :devices, :ip_address, :string
    add_column :devices, :mac_address, :string
    add_column :devices, :manufacturer, :string
    add_column :devices, :model, :string
    add_column :devices, :readable_version, :string
    add_column :devices, :system_name, :string
    add_column :devices, :system_version, :string
    add_column :devices, :timezone, :string
    add_column :devices, :total_disk_capacity, :string
    add_column :devices, :total_memory, :string
    add_column :devices, :unique_id, :string
    add_column :devices, :user_agent, :string
    add_column :devices, :version, :string
    add_column :devices, :is_24_hour, :string
    add_column :devices, :is_emulator, :string
    add_column :devices, :is_pin_or_fingerprint_set, :string
    add_column :devices, :is_tablet, :string
    add_column :devices, :has_notch, :string
    add_column :devices, :is_landscape, :string
    add_column :devices, :device_type, :string
  end
end
