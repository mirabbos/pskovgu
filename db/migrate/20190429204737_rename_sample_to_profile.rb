class RenameSampleToProfile < ActiveRecord::Migration[5.2]
  def change
    remove_column :samples, :price
    remove_column :samples, :quantity
    remove_column :samples, :some_date
    remove_column :samples, :float_number
    add_column :samples, :first_name, :string
    add_column :samples, :last_name, :string
    add_column :samples, :patronymic, :string
    add_column :samples, :faculty, :string
    add_column :samples, :gender, :string
    add_column :samples, :telephone, :string
    add_column :samples, :address, :string
    add_column :samples, :uploaded_files, :string
    rename_table :samples,:profile
  end
end
