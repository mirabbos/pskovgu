class RenameUserGenderToFaculty < ActiveRecord::Migration[5.2]
  def change
    rename_column :users, :gender, :faculty
  end
end
