class CreateLocationsTable < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.string :speed
      t.string :longitude
      t.string :latitude
      t.string :accuracy
      t.string :heading
      t.string :altitude
      t.string :altitudeAccuracy
      t.references :device, foreign_key: true

      t.timestamps
    end
  end
end
