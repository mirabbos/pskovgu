class RemoveExtraInfoFromDevices < ActiveRecord::Migration[5.2]
  def change

    remove_column :devices, :user_agent, :string
    remove_column :devices, :application_name, :string
    remove_column :devices, :battery_level, :string
    remove_column :devices, :brand, :string
    remove_column :devices, :build_number, :string
    remove_column :devices, :bundle_id, :string
    remove_column :devices, :carrier, :string
    remove_column :devices, :device_country, :string
    remove_column :devices, :device_id, :string
    remove_column :devices, :device_locale, :string
    remove_column :devices, :device_name, :string
    remove_column :devices, :font_scale, :string
    remove_column :devices, :free_disk_storage, :string
    remove_column :devices, :ip_address, :string
    remove_column :devices, :mac_address, :string
    remove_column :devices, :manufacturer, :string
    remove_column :devices, :model, :string
    remove_column :devices, :readable_version, :string
    remove_column :devices, :system_name, :string
    remove_column :devices, :system_version, :string
    remove_column :devices, :timezone, :string
    remove_column :devices, :total_disk_capacity, :string
    remove_column :devices, :total_memory, :string
    remove_column :devices, :unique_id, :string
    remove_column :devices, :version, :string
    remove_column :devices, :is_24_hour, :string
    remove_column :devices, :is_pin_or_fingerprint_set, :string
    remove_column :devices, :is_tablet, :string
    remove_column :devices, :has_notch, :string
    remove_column :devices, :is_landscape, :string
    remove_column :devices, :device_type, :string
    remove_column :devices, :is_emulator, :string

    add_column :devices, :installationId, :string
    add_column :devices, :expoVersion, :string
    add_column :devices, :appOwnership, :string
    add_column :devices, :systemName, :string
    add_column :devices, :systemVersion, :string
    add_column :devices, :deviceName, :string
    add_column :devices, :deviceYearClass, :string
    add_column :devices, :sessionId, :string
    add_column :devices, :isDevice, :string
    add_column :devices, :udid, :string

  end
end
