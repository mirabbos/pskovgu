class CreateObjectsTable < ActiveRecord::Migration[5.2]
  def change
    create_table :samples do |t|
      t.string :name
      t.text :description
      t.string :image_url
      t.decimal :price, precision: 5, scale: 2, default: 0
      t.integer :quantity
      t.boolean :is_active, default: true
      t.datetime :some_date
      t.float :float_number

      t.references :user, foreign_key: true
      t.timestamps
    end
  end
end
