class RemoveLocationFieldsFromDevice < ActiveRecord::Migration[5.2]
  def change

    remove_column :devices, :speed
    remove_column :devices, :longitude
    remove_column :devices, :latitude
    remove_column :devices, :accuracy
    remove_column :devices, :heading
    remove_column :devices, :altitude
    remove_column :devices, :altitudeAccuracy

  end
end
