class AddNewColumnsToProfiles < ActiveRecord::Migration[5.2]
  def change
    add_column :profiles, :work_date, :string
    add_column :profiles, :old_work_date, :string
    add_column :profiles, :last_work_date, :string
    add_column :profiles, :first_position, :string
    add_column :profiles, :second_position, :string
    add_column :profiles, :third_position, :string
    add_column :profiles, :first_workplace, :string
    add_column :profiles, :second_workplace, :string
    add_column :profiles, :third_workplace, :string
    add_column :profiles, :vk, :string
    add_column :profiles, :facebook, :string
    add_column :profiles, :instagram, :string
    add_column :profiles, :course_year, :string
    add_column :profiles, :technical_skills, :string
    add_column :profiles, :cultural_skills, :string
    add_column :profiles, :scientific_skills, :string
    add_column :profiles, :general_skills, :string
    add_column :profiles, :achievement_docs, :string
  end
end
